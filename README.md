# MSRC registration toolbox
This python toolbox is performs registration between serial section microscopy images in several ways to match IMS experimental goals. As it stands this library is largely patchwork and can provide multipurpose outputs for downstream analysis in other software but does not do everything all in one.

## Use cases
1. Registration of microscopy data together
2. Generate of IMS registration masks for explicit laser ablation to IMS pixel registration.
3. Extraction of overlapping IMS pixels determined by high-resolution microscopy resolution
4. Preparation via Registration & export of Bruker flexImaging format annotations for next generation histology directed experiments.
